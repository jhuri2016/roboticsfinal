function [ z_state ] = getFlockPosition()
% Gets XYZ position of flock of birds relative to the  base
% of the cython robot. Uses two birds, one at the base of the robot and
% one free flying. Position is relative to the base bird.
%
%
%
%
global s;

count = 1;
numBirds = 4;
flock_data = zeros(3,10); %initialize our matrix
while count <= 10
    %Send request
    fprintf(s,'B');
    %Get response
    numBytes = 13;
    [birdBytes, numRead] = fread(s,numBytes*numBirds,'uint8');
    %Parse response
    if numRead < numBytes*numBirds
        msg = sprintf('The number of bytes read [%d] was fewer than required [%d] \n',...
            numRead,numBytes*numBirds);
        disp(msg)
        continue
    end

    [pos, ang] = Inputs.FlockOfBirds.parseBytesPositionAngles(birdBytes);

    %for i = 1:numBirds
%     for i = [2 4]
%         fprintf('Bird %i\tX:%+6.3f\tY:%+6.3f\tZ:%+6.3f\t',i,pos(:,i));
%         fprintf('Rz:%+6.1f\tRy:%+6.1f\tRx:%+6.1f\n',ang(:,i)*180/pi);
%     end

    %begin our code to do transform
    %Bird2 is under the robotic arm
    %Bird4 is our target
    x2 = pos(1,2);
    y2 = pos(2,2);
    z2 = pos(3,2);
    x4 = pos(1,4);
    y4 = pos(2,4);
    z4 = pos(3,4);

    T = [1 0 0 x2; 0 1 0 y2; 0 0 1 z2; 0 0 0 1];

    newXYZ = T^-1 * [x4;y4;z4;1];
    newXYZ(1,1) = -1*newXYZ(1,1); %change sign on x to match robot's coordinate frame
    newXYZ(3,1) = abs(newXYZ(3,1)); %set z to always be positive
    flock_data(:,count) =  newXYZ(1:3,1);
    
    count = count + 1;
end

out = mean(flock_data,2);

final = out*1000; %output in millimeters

curX = final(1,1)*1000;
curY = final(2,1)*1000;
curZ = final(3,1)*1000;

HIGH_THRESHOLD = 85;
MID_THRESHOLD = 55;
LOW_THRESHOLD = 0;


if curZ > HIGH_THRESHOLD
    fprintf('HIGH Z\n');
    z_state = 3;
elseif curZ < HIGH_THRESHOLD && curZ > MID_THRESHOLD
    fprintf('MID Z\n');
     z_state = 2;
else
    fprintf('LOW Z\n');
     z_state = 1;
end
                


end

