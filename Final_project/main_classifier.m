%-------------------------------------------------------------------------
% EN.525.786.91.SP16 - Human Robotics Interaction
% School     : Johns Hopkins University 
% Instructors: Jeff Lesho & Bobby Armiger
% Autohor    : Baran David Sahin
%-------------------------------------------------------------------------
% Date       : 04/01/2016
% Assignment : FINAL PROJECT
%-------------------------------------------------------------------------
% Function   : MAIN_CLASSIFIER - Returns classified action from the MYO
%                     
%-------------------------------------------------------------------------
%
%
%-------------------------------------------------------------------------
function c_action = main_classifier()
%-------------------------------------------------------------------------
% COMMANDS to CLASSIFY
%-------------------------------------------------------------------------
% noop - rest (do not do anything)
% gohome
% pick1
% pick2
% pick3
% drop tool
% track
% perform task
%-------------------------------------------------------------------------
% Some code here to get input from MYO...
% Simulate for now with a random value returned to the main program
c_action = round(8*rand);
%-------------------------------------------------------------------------
if (c_action == 0)
    c_action_str = 'rest-noop';
elseif (c_action == 1)
    c_action_str = 'go home';
elseif(c_action == 2)
    c_action_str = 'pick tool 1';
elseif(c_action == 3)
    c_action_str = 'pick tool 2';
elseif(c_action == 4)
    c_action_str = 'pick tool 3';
elseif(c_action == 5)
    c_action_str = 'drop tool';
elseif(c_action == 6)
    c_action_str = 'track';
elseif(c_action == 7)
    c_action_str = 'perform task';
else
    c_action_str = 'unknown';
end

msg = strcat(' - CLASS::User input classified as action :: ',c_action_str);
logme(msg);
end