%-------------------------------------------------------------------------
% EN.525.786.91.SP16 - Human Robotics Interaction
% School     : Johns Hopkins University 
% Instructors: Jeff Lesho & Bobby Armiger
% Autohor    : Baran David Sahin
%-------------------------------------------------------------------------
% Date       : 04/01/2016
% Assignment : FINAL PROJECT
%-------------------------------------------------------------------------
% Function   : Pick up tool 1 - using test_goto function for now
%              Same function used to "DROP" tool 1 as well.
%              Since it would be the same path of action but the gripper 
%              action is reversed.
%              % 1 - PICK TOOL
%              % 2 - DROP TOOL
%-------------------------------------------------------------------------
%
%
%-------------------------------------------------------------------------
function pickup_1(tool_action,tool_id)
   global hCyton;
   global hCytonControls;
   
   goto_home();%HOME - might not need it.
   %Operate Hand - CHECK THE ROBOT MIGHT BE REVERSED
   q_grip    = hCyton.hPlant.CurrentPosition;
   if(tool_action == 1)
      q_grip(8) = 1.0;
   end
   if(tool_action == 2)
      q_grip(8) = 0.0; 
   end
   hCyton.setJointParameters(q_grip);
   pause(2);

   pause(1);
   % Should consider setting some constraints here with the commanded
   % positions. Such as set shoulder roll to avoid the cables etc.
   % constraints goes here:
   % INITIAL HANDHOLDING
   %  - ROLL SHOULDER BASE TO INITIAL POINT TO THE TOOL HERE
   %  - PITCH ELBOW SLIGHTLY FORWARD TO ENSURE ELBOW DOWN SOLUTION
   q_current    = hCyton.hPlant.CurrentPosition;
   if(tool_id == 1)
      q_current(1) = 0.0  ; %FOR TOOL ONE  
   elseif(tool_id == 2)
      q_current(1) = 0.5  ; %FOR TOOL TWO  
   elseif(tool_id == 3)
      q_current(1) = -0.5  ; %FOR TOOL THREE 
   end
   
   q_current(4) = -0.4; %ELBOW DOWN
   hCyton.setJointParameters(q_current);
   pause(1);
   
   %Set writst center with initial traj computed in place 
   T_0_N = hCyton.hControls.getT_0_N(5);
   point_home  = T_0_N(1:3,4)';
   
   %WORK THIS!
   if(tool_id == 1)
      point_tool1 = [270 5 130];  % LIKELY TO CHANGE IN LAB
   elseif(tool_id == 2)
      point_tool1 = [270 125 130]; % LIKELY TO CHANGE IN LAB
   elseif(tool_id == 3)
      point_tool1 = [270 -125 130];  % LIKELY TO CHANGE IN LAB
   end
   
   [traj] = return_traj(point_home,point_tool1); 
   length(traj)
   
   %GOTO COMMANDS TO MOVE WRIST CENTER (5th joint)
   for i = 1:length(traj)
       test_goto(hCyton,hCytonControls,traj(i,:),5)
       %test_goto_wo_elbow(hCyton,hCytonControls,traj(i,:),5)
       %pause(0.05)
   end
   pause(2)
   %Set gripper in place (tip down the wrist)
   q      = hCyton.hPlant.CurrentPosition;
   q(6,1) = -1.8;
   hCyton.setJointParameters(q);
   pause(3);
   
   % GRAB WRIST center location and draw traj from this to tool (straight
   % down)
   T_0_N = hCyton.hControls.getT_0_N(5);
   point_current       = T_0_N(1:3,4)';
   point_tool_final    = point_current; %location likely to change in lab.
   point_tool_final(3) = point_tool_final(3) - 40; %Down placement
   
   [traj] = return_traj(point_current,point_tool_final);
   %Now solve to the hand end - 7th joint
   for i = 1:length(traj)
       test_goto_se(hCyton,hCytonControls,traj(i,:),5)
       %test_goto_se_wo_elbow(hCyton,hCytonControls,traj(i,:),5)
       %pause(0.05)
   end
   
   pause(3);
   %Operate Hand- CHECK THE ROBOT MIGHT BE REVERSED
   q_grip    = hCyton.hPlant.CurrentPosition;

   if(tool_action == 1)
      q_grip(8) = 0.0;
   end
   
   if(tool_action == 2)
      q_grip(8) = 1.0; 
   end
   hCyton.setJointParameters(q_grip);
   
   
   %Return back home with tool
   pause(2)
   goto_home();
end