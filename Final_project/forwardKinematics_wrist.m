function [A, T, T_4_7] = forwardKinematics_wrist(a,d,q)
            % No idea why I need this?!?
            import Presentation.CytonI.*
            % Computer forward Kinematics for Cyton
            % Returns 4x4 'A' matrices of each joint kinematics
            DH = @Presentation.CytonI.Robot.DH_transformation;
            
            % Computed statically from getDHParams()
            
            %d = [37.9300 -4.6200 145.0000 11.0000 175.0000 7.4000 -7.6500 0 0];
            %a = [0 0 0 0 0 67.7000 53.1500 8.0000 8.0000];
            
            % [A] matrices represent the kinematics of each joint relative
            % to the previous link (relative transformations)
            piOver2 = pi/2;
            A(:,:,1) = DH(a(1),  piOver2, d(1), q(1) );
            A(:,:,2) = DH(a(2), -piOver2, d(2), q(2) );
            A(:,:,3) = DH(a(3),  piOver2, d(3), q(3) );
            A(:,:,4) = DH(a(4), -piOver2, d(4), q(4) );
            A(:,:,5) = DH(a(5),  piOver2, d(5), q(5) );
            A(:,:,6) = DH(a(6),  piOver2, d(6), q(6) + piOver2 );
            A(:,:,7) = DH(a(7),  0, d(7), q(7) );

            if nargout > 1
                % Forward multiply the [A] matrices to get the [T] matrices
                % relative to the base frame (global transformations)
                T(:,:,1) = A(:,:,1);
                T(:,:,2) = T(:,:,1)*A(:,:,2);
                T(:,:,3) = T(:,:,2)*A(:,:,3);
                T(:,:,4) = T(:,:,3)*A(:,:,4);
                T(:,:,5) = T(:,:,4)*A(:,:,5);
                T(:,:,6) = T(:,:,5)*A(:,:,6);
                T(:,:,7) = T(:,:,6)*A(:,:,7);
            end
            
            T_4_7 = A(:,:,5)*A(:,:,6)*A(:,:,7);
end