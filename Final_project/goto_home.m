%-------------------------------------------------------------------------
% EN.525.786.91.SP16 - Human Robotics Interaction
% School     : Johns Hopkins University 
% Instructors: Jeff Lesho & Bobby Armiger
% Autohor    : Baran David Sahin
%-------------------------------------------------------------------------
% Date       : 04/01/2016
% Assignment : FINAL PROJECT
%-------------------------------------------------------------------------
% Function   : GOTO HOME - using test_goto function for now
%                     
%-------------------------------------------------------------------------
%
%
%-------------------------------------------------------------------------
function goto_home()
   global hCyton;
   global hCytonControls;
%      pause(0.5)
%   test_goto(hCyton,hCytonControls,[367.47 -57.17 175.05])
%      pause(0.5)
%   test_goto(hCyton,hCytonControls,[270.03 -43.98 383.07])
%      pause(0.5)
%   test_goto(hCyton,hCytonControls,[204.49 -40.68 444.20])
%      pause(0.5)
%   test_goto(hCyton,hCytonControls,[-7.65 -13.78 494.78])
   
   %q_home = [0 0 0 0 -1.4211 0 0 0]';
   q_home = [0 0 0 0 0 0 0 0]';
   
   %Don't drop what you have
   %q_grip    = hCyton.hPlant.CurrentPosition;
   
   %test_goto(hCyton,hCytonControls,[-7.65 -13.78 494.78],6);
   
   %q_home(8) = q_grip(8);
   
   hCyton.setJointParameters(q_home);
end