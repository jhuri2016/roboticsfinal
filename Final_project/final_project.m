%-------------------------------------------------------------------------
% EN.525.786.91.SP16 - Human Robotics Interaction
% School     : Johns Hopkins University 
% Instructors: Jeff Lesho & Bobby Armiger
% Autohor    : Baran David Sahin
%-------------------------------------------------------------------------
% Date       : 04/01/2016
% Assignment : FINAL PROJECT
%-------------------------------------------------------------------------
% Function   : MAIN - Main program to connect to hardware and ...<add more>
%                     
%-------------------------------------------------------------------------
%
%
clc
clear all
close all
%-------------------------------------------------------------------------
% GLOBALS etc.
%-------------------------------------------------------------------------
final_globals
%-------------------------------------------------------------------------
global S;
global abort_main;
global classifier_return;
global current_state;
global previous_state;
%global next_state;
%-------------------------------------------------------------------------
% STARTMEUP - launch the MiniVIE environment and to setup the CytonI model
startmeup
%-------------------------------------------------------------------------
S = txt_update;
%-------------------------------------------------------------------------
% LOGME - launch the control/log window
logme(' - MAIN::Starting program.');
%-------------------------------------------------------------------------
% CENTER THE ROBOT TO AWAIT FIRST COMMANDS
%goto_center();
%-------------------------------------------------------------------------
% MAIN LOOP
abort_main=0;
previous_state=1;   %memory for Statemachine
current_state = 1;
classifier_return = 0;
%-------------------------------------------------------------------------
while(abort_main==0)
 %-------------------------------------------------------------------------   
 % FSM eval - returns valid transitions to the program
 %-------------------------------------------------------------------------
 valid_next_states = eval_statemachine(current_state);
 
 %-------------------------------------------------------------------------   
 % Returns a classified output based on the user input   
 %-------------------------------------------------------------------------
 classifier_return = main_classifier();
 keyboard
 %-------------------------------------------------------------------------
 valid_transition = cmd_validity_check(valid_next_states);
 %-------------------------------------------------------------------------
 if(valid_transition == 1)
     previous_state = current_state;
     current_state = execute_command(classifier_return);
 end
 classifier_return = 0;
end
%-------------------------------------------------------------------------


