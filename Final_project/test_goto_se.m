%-------------------------------------------------------------------------
% EN.525.786.91.SP16 - Human Robotics Interaction
% School     : Johns Hopkins University 
% Instructors: Jeff Lesho & Bobby Armiger
% Autohor    : Baran David Sahin
%-------------------------------------------------------------------------
% Date       : 02/06/2016
% Assignment : Miscellaneous
%-------------------------------------------------------------------------
% Function   : TEST_GOTO -   
%                       
%-------------------------------------------------------------------------
%
%
%-------------------------------------------------------------------------
% For now hardcode the test position.
% Later we can be slick about this.
%pos = [-40 100 350];
function test_goto_se(hCyton,hCytonControls,pos,N_th_joint)
%-------------------------------------------------------------------------
if nargin < 1
   error('Must input desired end effector position');
end
%-------------------------------------------------------------------------
% No idea why I need this?!?
import Presentation.CytonI.*
%-------------------------------------------------------------------------
% Jacobian endpoint solution
pTarget = pos(:);
%-------------------------------------------------------------------------            
% get DH params:
[T_0_n, a, d] = hCytonControls.getDHParams();
%-------------------------------------------------------------------------
% Set some defaults for the overall behavior of -GOTO-
% Later might want to cosider making some of them user inputs.
errorMag   = 10;
dt         = 0.05;
TIMEOUT    = 0.25;
isTimeout  = false;
isMoving   = true; 
t0         = clock;
N          = N_th_joint;
%-------------------------------------------------------------------------
% Beginning of the main move loop
while errorMag > 1 && ~isTimeout && isMoving
%%
  q = hCyton.hPlant.CurrentPosition;
  q_to_set = q;
   
  J = hCytonControls.symJacobian(a(6),a(7),d(2),d(3),d(4),d(5),d(6),d(7),q(1),q(2),q(3),q(4),q(5),q(6),q(7));
  
  %J = hCytonControls.symJacobian(a(6),d(2),d(3),d(4),d(5),d(6),q(1),q(2),q(3),q(4),q(5),q(6));
  
  
  %J = obj.hCyton.hControls.symJacobian(a(6),a(7),d(2),d(3),d(4),d(5),d(6),d(7),q(1),q(2),q(3),q(4),q(5),q(6),q(7));
  %J = obj.hControls.symJacobianWrist(d(2),d(3),d(4),d(5),q(1),q(2),q(3),q(4));
                
  %N = 7;
  %N = 6; %Leave room for orientation - NOW AN INPUT ARGUMENT
  
  T_0_N = hCyton.hControls.getT_0_N(N);
  pEE = T_0_N(1:3,4);
  pError = pTarget - pEE;
                
  errorMag = norm(pError);
               
  P = 10; %GAIN
  vMove = pError * P;
                
  q_dot = pinv(J) * vMove;
  
  % Shrink q_dot appropriately as well to allow for orientation of gripper
  q_dot = q_dot(1:N);
               
  % Apply joint speed limits
  %q_dot = min(abs(q_dot),1) .* sign(q_dot);
  if any(q_dot > 1)
     q_dot = q_dot./max(abs(q_dot));
  end
                
  q_dot = min(abs(q_dot),1) .* sign(q_dot);
                
  isMoving = any(abs(q_dot) > 0.1) || ...
             any(~hCyton.hPlant.isMoveComplete);
  %~all(abs(q_dot) > 0.1) || ~all(obj.hCyton.hPlant.isMoveComplete);
                
                
  % Apply Joint limits
  q(1) = max(min(q(1),7*pi/8),-7*pi/8);
  q(2) = max(min(q(2),pi/2),-pi/2);
  q(4) = max(min(q(4),pi/2),-pi/2);
  q(6) = max(min(q(6),pi/3),-pi/3);

  q(1:N) = q(1:N) + (q_dot*dt);
  q_dot';
  
  %Only move PITCH JOINTS 2,4 and 6
  q_to_set(2) = q(2);
  q_to_set(4) = q(4);
  q_to_set(6) = q(6);
  
  q_to_set(6) = -1*(pi() - abs(q_to_set(2)) - abs(q_to_set(4))); 
  
  hCyton.setJointParameters(q_to_set);
                
  drawnow
                
  isTimeout = (etime(clock,t0) > TIMEOUT);
end
%END of the main move loop.
%-------------------------------------------------------------------------
% Check some values to find out how the move went
% Perhaps make these return values if -GOTO- is to be a function later.
if isTimeout
   fprintf('Timed Out.  Error is: %6.2f\n',errorMag);
else
   fprintf('Move Complete.  Error is: %6.2f\n',errorMag);
end
%-------------------------------------------------------------------------
% END funtion GOTO
end

 