%-------------------------------------------------------------------------
% EN.525.786.91.SP16 - Human Robotics Interaction
% School     : Johns Hopkins University 
% Instructors: Jeff Lesho & Bobby Armiger
% Autohor    : Baran David Sahin
%-------------------------------------------------------------------------
% Date       : 04/01/2016
% Assignment : FINAL PROJECT
%-------------------------------------------------------------------------
% Function   : Command Validity Check
%                     
%-------------------------------------------------------------------------
%
% valid_transition = 0|1 (Flag to indicate if the transition is valid
%-------------------------------------------------------------------------
function valid_transition = cmd_validity_check(valid_next_states)
%-------------------------------------------------------------------------
global classifier_return;
c_return = classifier_return;
%-------------------------------------------------------------------------
I = ismember(c_return,valid_next_states);
if(I ~= 0)
   valid_transition = 1;
   logme(' - CMDVALID::Received and classified valid command from user.');
else
   valid_transition = 0;
   logme(' - CMDVALID::FAILED to classify a valid command from user.'); 
end

end