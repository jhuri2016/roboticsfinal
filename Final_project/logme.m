%-------------------------------------------------------------------------
% EN.525.786.91.SP16 - Human Robotics Interaction
% School     : Johns Hopkins University 
% Instructors: Jeff Lesho & Bobby Armiger
% Autohor    : Baran David Sahin
%-------------------------------------------------------------------------
% Date       : 04/01/2016
% Assignment : Final project
%-------------------------------------------------------------------------
% Function   : Log events to keep track of what the program is doing
%                        
%-------------------------------------------------------------------------
%
%
%-------------------------------------------------------------------------
function logme(msg)
global S;
t = datestr(now);
B = strcat(1,t,msg);  % Update with character data.
set(S.tx,'string',cat(1,get(S.tx,'string'),{B}))
end
