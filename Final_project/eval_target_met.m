%-------------------------------------------------------------------------
% EN.525.786.91.SP16 - Human Robotics Interaction
% School     : Johns Hopkins University 
% Instructors: Jeff Lesho & Bobby Armiger
% Autohor    : Jaime Zabala
%-------------------------------------------------------------------------
% Date       : 04/01/2016
% Assignment : FINAL PROJECT
%-------------------------------------------------------------------------
% Function   : XYZ Validity Check
%             Returns 1 if the passed XYZ paramter is within a spherical tolerance
%
%-------------------------------------------------------------------------

function valid_transition = eval_target_met(XYZ)
%-------------------------------------------------------------------------
   global hCyton;
   global hCytonControls;
   
tolerance = 2;

XYZTarget = XYZ(:);

T_0_N = hCyton.hControls.getT_0_N(7);

curr_pos  = T_0_N(1:3,4)';

valid_transition = 1;

%CONSIDER USING THE PEE AND PE ERROR FROM TEST_GOTO


if(XYZ(1) > (curr_pos(1) + tolerance))
    valid_transition = 0;
elseif(XYZ(1) < (curr_pos(1) - tolerance))
    valid_transition = 0;
elseif(XYZ(2) > (curr_pos(2) + tolerance))
    valid_transition = 0;
elseif(XYZ(2) < (curr_pos(2) - tolerance))
    valid_transition = 0;
elseif(XYZ(3) > (curr_pos(3) + tolerance))
    valid_transition = 0;
elseif(XYZ(3) < (curr_pos(3) - tolerance))
    valid_transition = 0;    
end
    %-------------------------------------------------------------------------

end