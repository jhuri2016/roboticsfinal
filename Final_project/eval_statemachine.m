%-------------------------------------------------------------------------
% EN.525.786.91.SP16 - Human Robotics Interaction
% School     : Johns Hopkins University 
% Instructors: Jeff Lesho & Bobby Armiger
% Autohor    : Baran David Sahin
%-------------------------------------------------------------------------
% Date       : 04/01/2016
% Assignment : FINAL PROJECT
%-------------------------------------------------------------------------
% Function   : EVAL STATE MACHINE
%                     
%-------------------------------------------------------------------------
%
%
%-------------------------------------------------------------------------
function [valid_next_states,valid_next_command]=eval_statemachine(current_state)

valid_next_states=[0 0 0];

switch     current_state
    case    {1}
    % This is the Startup/Standby case
    % The valid next states for this case are:
    % - Return back to this state (State 1)
    % - Pick tools 1 or 2 (States 2 and 3)
           valid_next_states=[1,2,3];
    case   {2}
    % This is the PICK TOOL 1 state
    % The valid next states for this case are:
    % - Drop the tool back into place (State 4)
    % - Track FOB (State 5)
    % - State 1
           valid_next_states=[1,4,5];
    case   {3}
    % This is the PICK TOOL 2 state
    % The valid next states for this case are:
    % - Drop the tool back into place (State 5)
    % - Track FOB (State 6)
           valid_next_states=[1,4,5];
    case   {4}
    % This is the DROP TOOL state
    % The valid next states for this case are:
           valid_next_states=[1,2,3];
    case   {5}
    % This is the TRACK/MEET FOB state
    % The valid next states for this case are:
    % - Performs task
    % - State1
           valid_next_states=[1,5,6];
    case   {6}
    % This is the Perform Task state
    % The valid next states for this case are:
    % - State1
           valid_next_states=[1,6];     
    otherwise
           valid_next_states=[9];
end

end