%-------------------------------------------------------------------------
% EN.525.786.91.SP16 - Human Robotics Interaction
% School     : Johns Hopkins University 
% Instructors: Jeff Lesho & Bobby Armiger
% Autohor    : Baran David Sahin
%-------------------------------------------------------------------------
% Date       : 04/01/2016
% Assignment : FINAL PROJECT
%-------------------------------------------------------------------------
% Function   : Pick up tool 1 - using test_goto function for now
%                     
%-------------------------------------------------------------------------
%
%
%-------------------------------------------------------------------------
function drop_tool()
   global hCyton;
   global hCytonControls;
   global previous_state;
   
   goto_home();%HOME - might not need it.
   pause(1);
   %Should consider setting some constraints here with the commanded
   %positions. Such as set shoulder roll to avoid the cables etc.
   %constraints goes here:
   
   if (previous_state == 2)
       %AT HAND SHOULD BE TOOL 1
       pickup_1(2,1); %PICKUP FUNCTION BUT THE INPUT ARGUMENT 2 MEANS DROP
   end 
   if (previous_state == 3)
       %AT HAND SHOULD BE TOOL 2
       pickup_1(2,2); %PICKUP FUNCTION BUT THE INPUT ARGUMENT 2 MEANS DROP
   end 
   if (previous_state == 4)
       %AT HAND SHOULD BE TOOL 3
       pickup_1(2,3); %PICKUP FUNCTION BUT THE INPUT ARGUMENT 2 MEANS DROP
   end 
end