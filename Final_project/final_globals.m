%-------------------------------------------------------------------------
% EN.525.786.91.SP16 - Human Robotics Interaction
% School     : Johns Hopkins University 
% Instructors: Jeff Lesho & Bobby Armiger
% Autohor    : Baran David Sahin
%-------------------------------------------------------------------------
% Date       : 04/01/2016
% Assignment : FINAL PROJECT
%-------------------------------------------------------------------------
% Function   : GLOBALS - Define GLOBALS that the main program would need.
%                     
%-------------------------------------------------------------------------
%
%
%-------------------------------------------------------------------------
global logger;
global abort_main;
global classifier_return;
global hCyton;
global S;