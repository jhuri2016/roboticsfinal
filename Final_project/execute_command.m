%-------------------------------------------------------------------------
% EN.525.786.91.SP16 - Human Robotics Interaction
% School     : Johns Hopkins University 
% Instructors: Jeff Lesho & Bobby Armiger
% Autohor    : Baran David Sahin
%-------------------------------------------------------------------------
% Date       : 04/01/2016
% Assignment : FINAL PROJECT
%-------------------------------------------------------------------------
% Function   : Execute command from user
%                     
%-------------------------------------------------------------------------
%
% 
%-------------------------------------------------------------------------
function next_state = execute_command(input_command)
%-------------------------------------------------------------------------
% COMMANDS to CLASSIFY
%-------------------------------------------------------------------------
% gohome - 1
% pick1  - 2
% pick2  - 3 
% drop   - 4
% track  - 5
% perform- 6

switch     input_command
    case    {1}
          %go home and set the state to State 1
          logme(' - EXECCMD::Go home command executing.');
          next_state = 1;
          goto_home();
    case    {2}
          %set the next state to pick1
          next_state = 2;
          %commands to pick tool 1 from the tool box
          pickup_1(1,1);
    case    {3}
          %set the next state to pick2
          next_state = 3;
          %commands to pick tool 2 from the tool box
          pickup_1(1,2);        
    case    {4}
          %set the next state to drop tool
          next_state = 4;
          %commands to return the tool to the toolbox
          drop_tool();
    case    {5}
          %set the next state to track_target
          next_state = 5;
          %commands to track the FOB
          track_target();
    case    {6}
          %set the next state to perform task at the FOB target
          next_state = 6;
          %commands to perform task at the FOB target
          %perform_task();
    otherwise
          next_state = 9;
end

end