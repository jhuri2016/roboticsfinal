%-------------------------------------------------------------------------
% EN.525.786.91.SP16 - Human Robotics Interaction
% School     : Johns Hopkins University 
% Instructors: Jeff Lesho & Bobby Armiger
% Autohor    : Baran David Sahin
%-------------------------------------------------------------------------
% Date       : 02/06/2016
% Assignment : Final project
%-------------------------------------------------------------------------
% Function   : Start up - To be used to launch the MiniVIE environment
%                         and to setup the CytonI model
%-------------------------------------------------------------------------
%
%
%-------------------------------------------------------------------------
global hCyton;
global hCytonControls;
global s;
% Change path the the SOURCE and run the configurePath command
%cd ../
MiniVIE.configurePath();
%-------------------------------------------------------------------------
% Change path back into the working environment to begin development
%cd Final_project
%-------------------------------------------------------------------------
% Now, import the MiniVIE Cyton functions
import Presentation.CytonI.*
delete(instrfindall)
%-------------------------------------------------------------------------
% Launch the cyton virtual integration environment (VIE)
hCyton = CytonI
%-------------------------------------------------------------------------
% Launch the joint control gui
hCyton.guiJointSliders
%-------------------------------------------------------------------------
% Setup Display
hCytonDisplay = hCyton.hDisplay;
%-------------------------------------------------------------------------
% Setup Controls 
hCytonControls = hCyton.hControls;
% Get dh param constants
q  = zeros(1,6);
d  = zeros(1,6);
a  = zeros(1,6);
[T_0_n, a, d] = hCytonControls.getDHParams();
q = hCyton.hPlant.CurrentPosition;
% Get Jacobian at the current position
J_ = hCytonControls.symJacobianFull(a(6),d(2),d(3),d(4),d(5),d(6),q(1),q(2),q(3),q(4),q(5),q(6));
%-------------------------------------------------------------------------
current_spot = hCyton.T_0_EndEffector(1:3,4)


%% Flock of Birds setup
s = serial('COM7');              % default = 'COM1'
set(s,'BaudRate',115200);        % default = 9600
set(s,'RequestToSend','off');    % default = on
set(s,'DataTerminalReady','on'); % default = on
set(s,'InputBufferSize',512*10); % default = 512
set(s,'Timeout',0.5);            % default = 10

fopen(s);
 %%Set initial mode
numBirds = 4;

ERT = 1;  % ERT = 0 No Extended range transmitter

% Set mode
for i = 1:numBirds+ERT
    fwrite(s,[240+i 89]); % set to gather position and angles
end

% no light response
% if error, blinking lights -- ensure another serial port not
% holding RequestToSend high

%%Autoconfig
pause(0.3);   % 300 misec delay required before AutoConfig commands (p. 83)
fwrite(s,[240+1 80 50 numBirds+ERT]);  % autoconfig for Master => bird 1
pause(0.3);   % 300 misec delay required after AutoConfig commands (p. 83)

% lights should go on

%%GROUP MODE
% PARAMETERnumber = 35
% The GROUP MODE command is only used if you have multiple Birds working together
% in a Master/Slave configuration and you want to get data from all the Birds by talking to
% only the Master Bird.

% turn group mode on if necessary
fwrite(s,[240+1 80 35 1])

