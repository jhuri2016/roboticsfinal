%-------------------------------------------------------------------------
% EN.525.786.91.SP16 - Human Robotics Interaction
% School     : Johns Hopkins University 
% Instructors: Jeff Lesho & Bobby Armiger
% Autohor    : Jaime Zabala
%-------------------------------------------------------------------------
% Date       : 04/01/2016
% Assignment : FINAL PROJECT
%-------------------------------------------------------------------------
% Function   : goto the provided XYZ
%-------------------------------------------------------------------------
%
%
%-------------------------------------------------------------------------
function goto_pos(XYZ)
   global hCyton;
   global hCytonControls;

   
   %q_current    = hCyton.hPlant.CurrentPosition;
   %q_current(4) = q_current(4)-2.4; %ELBOW DOWN
   %hCyton.setJointParameters(q_current);
  
   pause(1);
   
   
   T_0_N = hCyton.hControls.getT_0_N(7);

   curr_pos  = T_0_N(1:3,4)';

   %get trajectory from current pos to new XYZ
   [traj] = return_traj(curr_pos,XYZ); 
   length(traj)
   
   %GOTO COMMANDS TO MOVE WRIST CENTER (5th joint)
   for i = 1:length(traj)
       test_goto(hCyton,hCytonControls,traj(i,:),7)
       %test_goto(hCyton,hCytonControls,traj(i,:),5)
       %pause(0.5)
   end
end