%-------------------------------------------------------------------------
% EN.525.786.91.SP16 - Human Robotics Interaction
% School     : Johns Hopkins University 
% Instructors: Jeff Lesho & Bobby Armiger
% Autohor    : Baran David Sahin
%-------------------------------------------------------------------------
% Date       : 04/01/2016
% Assignment : FINAL PROJECT
%-------------------------------------------------------------------------
% Function   : Return traj - input two points return a traj
%                     
%-------------------------------------------------------------------------
%
%
%-------------------------------------------------------------------------
function [traj] = return_traj(P1,P2)
% Their vertial concatenation is what you want
pts = [P1; P2];
% Because that's what line() wants to see    
line(pts(:,1), pts(:,2), pts(:,3));
xs = linspace(pts(1,1),pts(2,1));
ys = linspace(pts(1,2),pts(2,2));
zs = linspace(pts(1,3),pts(2,3));

[traj] = [xs;ys;zs]';
end
