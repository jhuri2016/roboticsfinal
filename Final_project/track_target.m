%-------------------------------------------------------------------------
% EN.525.786.91.SP16 - Human Robotics Interaction
% School     : Johns Hopkins University 
% Instructors: Jeff Lesho & Bobby Armiger
% Autohor    : Baran David Sahin
%-------------------------------------------------------------------------
% Date       : 04/01/2016
% Assignment : FINAL PROJECT
%-------------------------------------------------------------------------
% Function   : MAIN_CLASSIFIER - Returns classified action from the MYO
%                     
%-------------------------------------------------------------------------
%
%
%-------------------------------------------------------------------------

function pass_fail = track_target()
   global hCyton;
   global hCytonControls;
   pass_fail = 0;
   valid_check = 0;
   %goto_home();%HOME - should already be there
   
   % Assuming there is feedback at this point from the FOB system
   % in the global XYZ coordinates we should be able to use the GOTO 
   % commands in a loop until the FOB target stops mooving.
   %
   % Once the target stops (determined from a threshold in coordinates)
   % the Robot will assume that it met the target.
   target_met           = 0;
   point_track          = [0 0 0]; 
   point_num            = 0;
   previous_point_track = [0 0 0];
   current_xyz          = [0 0 0];
   workspace_xyz        = [2 -175 275];  % sample - NEED TO UPDATE
   position_1_low       = [2 -175 200];
   position_2_mid       = [2 -175 250];
   position_3_hi        = [2 -175 300];
   
   logme(' - TRACK::Starting track function.');
%   init_flock();              %DOESNT EXIST - initialize and start receiving FOB inputs
   q_current = hCyton.hPlant.CurrentPosition;
   q_current(4) = -0.95;
   hCyton.setJointParameters(q_current);
   pause(3);
   q_current = hCyton.hPlant.CurrentPosition;
   q_current(1) = -1.4;
   hCyton.setJointParameters(q_current);
   pause(3);
   q_current = hCyton.hPlant.CurrentPosition;
   q_current(2) = 0.5;
   hCyton.setJointParameters(q_current);
   pause(3);

   
   goto_pos(workspace_xyz);   % prime robot in the workspace '
   pause(10);
   
   while (target_met == 0)
       valid_check =0;
       point_num = getFlockPosition();  % - function that returns XYZ from FOB
       if (point_num == 1)
           point_track = position_1_low;
       elseif (point_num == 2)
           point_track = position_2_mid;
       elseif (point_num == 3)
           point_track = position_3_hi;
       end
           
       keyboard;
       
       %valid_check = xyz_validity_check(point_track);  simplified
       valid_check = 1
       
       if (valid_check == 1)
           logme(' - TRACK::Tracking...');
           target_met = eval_target_met(point_track); % have we met the xyz  
            if (target_met == 1)
                logme(' - TRACK:: target met!.');  % set exit criteria and pass status
                pass_fail = 1;
            elseif (target_met ==0) 
                logme(' - TRACK:: Moving to target!.'); 
                goto_pos(point_track);   %move robot to XYZ
            else
                logme(' - TRACK:: unknown target met parameter!.'); 
            end
            
       elseif (valid_check ==0) 
           logme(' - TRACK::Invalid XYZ parameter.');
       else
           logme(' - TRACK::Unknown validity parameter.');
       end
   end
   
   %SET THE EXIT
end