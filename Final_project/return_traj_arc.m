%-------------------------------------------------------------------------
% EN.525.786.91.SP16 - Human Robotics Interaction
% School     : Johns Hopkins University 
% Instructors: Jeff Lesho & Bobby Armiger
% Autohor    : Baran David Sahin
%-------------------------------------------------------------------------
% Date       : 04/01/2016
% Assignment : FINAL PROJECT
%-------------------------------------------------------------------------
% Function   : Return traj - input two points return a traj
%                     
%-------------------------------------------------------------------------
%
%
%-------------------------------------------------------------------------
function [traj] = return_traj_arc(P1,P2)
% Their vertial concatenation is what you want
pts = [P1; P2];

x0 = 0; %Center of the square to define the arc path
y0 = 0; %Center of the square to define the arc path
z0 = 0; %Center of the square to define the arc path

x1 = pts(1,1);
x2 = pts(2,1);
y1 = pts(1,2);
y2 = pts(2,2);
z1 = pts(1,3);
z2 = pts(2,3);

 v1 = [x1-x0;y1-y0;z1-z0]; % Vector from center to 1st point
 r = norm(v1); % The radius
 v2 = [x2-x0;y2-y0;z2-z0]; % Vector from center to 2nd point
 v3 = cross(cross(v1,v2),v1); % v3 lies in plane of v1 & v2 and is orthog. to v1
 v3 = r*v3/norm(v3); % Make v3 of length r
 % Let t range through the inner angle between v1 and v2
 t = linspace(0,atan2(norm(cross(v1,v2)),dot(v1,v2)));
 v = v1*cos(t)+v3*sin(t); % v traces great circle path, relative to center
 
 %plot3(v(1,:)+x0,v(2,:)+y0,v(3,:)+z0) % Plot it in 3D
 
 [traj] = [v(1,:)+x0;v(2,:)+y0;v(3,:)+z0]';

end
