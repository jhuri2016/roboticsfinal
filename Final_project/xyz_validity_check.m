%-------------------------------------------------------------------------
% EN.525.786.91.SP16 - Human Robotics Interaction
% School     : Johns Hopkins University 
% Instructors: Jeff Lesho & Bobby Armiger
% Autohor    : Jaime Zabala
%-------------------------------------------------------------------------
% Date       : 04/01/2016
% Assignment : FINAL PROJECT
%-------------------------------------------------------------------------
% Function   : XYZ Validity Check
%             Returns 1 if the passed XYZ paramter is within a spherical tolerance
%
%-------------------------------------------------------------------------

function valid_transition = xyz_validity_check(XYZ)
%-------------------------------------------------------------------------
workspace_cube_len = 75;
workspace_center = [2 -175 275];  % Need to update

valid_transition = 1;

if(XYZ(1) > (workspace_center(1) + workspace_cube_len))
    valid_transition = 0;
elseif(XYZ(1) < (workspace_center(1) - workspace_cube_len))
    valid_transition = 0;
elseif(XYZ(2) > (workspace_center(2) + workspace_cube_len))
    valid_transition = 0;
elseif(XYZ(2) < (workspace_center(2) - workspace_cube_len))
    valid_transition = 0;
elseif(XYZ(3) > (workspace_center(3) + workspace_cube_len))
    valid_transition = 0;
elseif(XYZ(3) < (workspace_center(3) - workspace_cube_len))
    valid_transition = 0;    
end
    %-------------------------------------------------------------------------

end