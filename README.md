# README #

### How do I get set up? ###

I used this tutorial to clone this git repo into MATLAB since I've never done that.
It's pretty straight forward.

http://www.mathworks.com/help/matlab/matlab_prog/retrieve-from-git-repository.html

You'll then probably have to adjust the 'cd' commmands within startmeup.m depending on where you mapped this to in your directory structure.

-Mark
